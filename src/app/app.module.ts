import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DataTableComponent } from './components/data-table/data-table.component';
import { DataInputComponent } from './components/data-input/data-input.component';
import { AppShellComponent } from './components/app-shell/app-shell.component';
import { JsonViewComponent } from './components/json-view/json-view.component';

const appRoutes: Routes = [
  { path: '', component: DataInputComponent },
  { path: 'table', component: DataTableComponent },
  { path: 'json-view', component: JsonViewComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    DataTableComponent,
    DataInputComponent,
    AppShellComponent,
    JsonViewComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
