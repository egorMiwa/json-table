import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../../data.service'
@Component({
  selector: 'app-json-view',
  templateUrl: './json-view.component.html',
  styleUrls: ['./json-view.component.css']
})
export class JsonViewComponent implements OnInit {
  data = "dasdasdsa";
  constructor(private service: DataService, private router: Router) { }

  ngOnInit(): void {
    this.data = JSON.stringify(this.service.getData());
  }
  onBackClick(): void {
    this.router.navigate(['/'])
  }
}
