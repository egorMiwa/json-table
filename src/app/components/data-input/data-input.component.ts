import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data.service';

@Component({
  selector: 'app-data-input',
  templateUrl: './data-input.component.html',
  styleUrls: ['./data-input.component.css']
})
export class DataInputComponent implements OnInit {

  constructor(private router: Router,private service: DataService) { }
  // textValue = 'initial value';
    ngOnInit(): void {
  }
  onTextSubmit(textValue): void {
    this.service.saveStringAsObject(textValue)
    this.router.navigate(['/table']);
  }
}
