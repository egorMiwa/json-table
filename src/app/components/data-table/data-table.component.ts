import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../../data.service'
@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css']
})
export class DataTableComponent implements OnInit {

  constructor(private service: DataService, private router: Router) { }
  data: any;
  displayedColumns = []
  ngOnInit(): void {
    this.displayedColumns = this.service.getTabeRowNames();
    this.data = this.service.getData();
    console.log(this.data)
    this.data.forEach(element => {
      element.isEditable = false
    });
  }
  onBackClick(): void {
    this.router.navigate(['/'])
  }
  onShowClick(): void {
    this.data.forEach(element => {
      delete element.isEditable
    });
    this.router.navigate(['/json-view'])
  }
  onEditClick(item): void {
    item.isEditable = true;
  }
  onSaveClick(item): void {
    this.service.updateData(this.data)
    item.isEditable = false
  }
}
