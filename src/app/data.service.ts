import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  data = [];
  getTabeRowNames() {
    let keys
    if (this.data.length > 0) {
      keys = Object.keys(this.data[0]);
    }
    return keys
  }
  getData() {
    return this.data
  }
  saveStringAsObject(data) {
    this.data = JSON.parse(data);
  }
  updateData(data) {
    this.data = data;
  }
  constructor() { }
}
